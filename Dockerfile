#Instalar

FROM registry.access.redhat.com/ubi9/ubi:9.4

RUN yum install -y haproxy

VOLUME ["/opt/haproxy"]

ENTRYPOINT ["haproxy"]

CMD ["-f", "/opt/haproxy/haproxy.cfg"]
